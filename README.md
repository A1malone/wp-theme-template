# wp-theme-template

This is a blank wordpress theme that will be used as a shell to build themes.

## Installation

Clone into theme folder.

I personally use [local](https://localwp.com/) to run/setup my wp environment.

## Usage

Replace wp-content folder or just replace the theme inside of your current one if you just plan to upload a theme without 'must use plugins'.

## reference

- [WP Docs](https://developer.wordpress.org/reference/)
- [Cheat Sheet](https://websitesetup.org/wordpress-cheat-sheet/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
